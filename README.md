# simple notes [![Python](https://www.python.org/static/community_logos/python-powered-w-70x28.png)](https://github.com/python/cpython)  [![GNU](https://i.imgur.com/aPzjPSS.png)](https://www.gnu.org/)
Super simple note-taking program

![simple-notes example](https://my.mixtape.moe/ikywwt.png "simple-notes example")

Above is an example, it may not reflect the latest version entirely. [Not that this will be updated all that often]
# <a name="Creators"></a>Creators
* Dev: [tiraboschi](http://steam.name/tiraboschi)

# <a name="Origins"></a>Origins
I'm really bored :p