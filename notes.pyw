from tkinter import *
import os
import time
root = Tk()
root.overrideredirect(True)
# Background Colors
light = '#%02x%02x%02x' % (240, 240, 240)
dark = '#%02x%02x%02x' % (41, 41, 41)
class WindowDraggable():

    def __init__(self, label):
        self.label = label
        label.bind('<ButtonPress-1>', self.StartMove)
        label.bind('<ButtonRelease-1>', self.StopMove)
        label.bind('<B1-Motion>', self.OnMotion)
        label.bind('<ButtonPress-3>', self.Close)

    def StartMove(self, event):
        self.x = event.x
        self.y = event.y

    def StopMove(self, event):
        self.x = None
        self.y = None

    def OnMotion(self,event):
        x = (event.x_root - self.x - self.label.winfo_rootx() + self.label.winfo_rootx())
        y = (event.y_root - self.y - self.label.winfo_rooty() + self.label.winfo_rooty())
        root.geometry("+%s+%s" % (x, y))

    def Close(self, event):
        optext = text.get("1.0",'end')
        with open("output.txt", "w") as text_file:
                text_file.write(optext)
        time.sleep(0.2)
        sys.exit()

# Text box
text = Text(root, width=30, height=15)
# Default text / read file
text.insert('end', "-=- Hello! Hold left click to move the window, and right click to close it! -=-\n-=- read the ini for more customization :D -=-")
file = open("output.txt", 'r')
data = file.read()
file.close()
text.delete(1.0, END)
text.insert('end', data)
text.pack()
# Ini reader
dm = "[darkmode]"
lm = "[lightmode]"
def darktheme():
   text.configure(background=dark, foreground='grey')

def lighttheme():
    text.configure(background=light)
try:
    ini = os.getcwd() + "\\notes.ini"
    inio = open(ini, 'r')
    check = inio.readlines()
    inio.close()
    for line in check:
        if str(dm) in line:
            darktheme()
        elif str(lm) in line:
            lighttheme()
except FileNotFoundError:
    pass
WindowDraggable(text)
root.mainloop()
